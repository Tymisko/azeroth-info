# Azeroth Info

A school project that has in purpose to practice students' skills in **HTML, CSS, JavaScript**, and **PHP** with **databases and SQL usage**. Practice these technologies was necessary to pass the vocational exam.

Azeroth Info is about World of Warcraft, more precisely about Azeroth, the main planet in this universe. This is a demo version of the website with news of the world of Azeroth.

I have created my own script in JavaScript to switch between articles without changing pages. This website has its mobile version as well and was created with the following responsive web design rules.

# Project presentation

## Main Page

<img src="./project-screenshots/main-page.jpg" width=5000px style="border-radius: 25px">

## Navigation bar

<img src="./project-screenshots/navigation-bar.jpg" width=1000px>

## Sample section

<img src="./project-screenshots/main-page-breaking-news.jpg" width=1000px>

<img src="./project-screenshots/watch-it-section.jpg" width=1000px>

## Sample subpage

<img src="./project-screenshots/style-subpage.jpg" width=1000px>

## Mobile Version

| Example Mobile Section                                                      | Main Page                                                                             | Mobile Navigation                                                                |
| --------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| <img src="./project-screenshots/mobile-version.jpg" width=250px height=450> | <img src="./project-screenshots/mobile-version-main-page.jpg" width=250px height=450> | <img src="./project-screenshots/mobile-version-menu.jpg" width=250px height=450> |

## Login System

<img src="./project-screenshots/login-system.jpg" width=1000px>

## Footer

<img src="./project-screenshots/footer.jpg" width=1000px>
